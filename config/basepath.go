/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package config

import (
	"fmt"
	"go/build"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// defaultBasePath contains the default base path used.
var defaultBasePath = getWorkingDir()

// basePath contains the path where this application searches for resources.
var basePath = defaultBasePath

// fallBackBasePath is the base path used during testing.
var fallBackBasePath = filepath.Join(getGoPath(), "src", "gitlab.com", "stp-team", "systemtestportal-webapp")

// neededFolders determines which folders need to be contained in th base path.
var neededFolders = []string{"templates", "static"}

func init() {
	// Work around so tests will still find the template files even if
	// the application isn't started normally.
	err := checkBasePath(basePath)
	if err != nil {
		basePath = fallBackBasePath
	}
}

// BasePath returns the configured base path.
func BasePath() string {
	return basePath
}

// getGoPath gets the GOPATH environment variable. If it isn't set
// it gets the default value for it.
func getGoPath() string {
	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		return build.Default.GOPATH
	}
	return gopath
}

// initBasePath is called after the configuration was loaded. And changes the basepath
// to given path.
func initBasePath(bpath string) {
	basePath = bpath
	err := checkBasePath(basePath)
	if err != nil {
		exitExplainingError(err, basePath)
	}
	err = os.Chdir(basePath)
	if err != nil {
		log.Fatalf("Unable to change working directory to basepath '%s': %s", basePath, err)
	}
	log.Printf("Using '%s' as base path.\n", basePath)
}

func exitExplainingError(err error, basePath string) {
	if _, ok := err.(requiredFolderError); ok {
		log.Fatalf("The given basepath (%s) doesn't contain the required folders (%v).\n"+
			"Please make sure to place the required folders you got when downloading stp "+
			"into the basepath or change "+
			"it to a different folder using the --basepath=<basepath> arguement.",
			basePath, neededFolders)
	} else {
		log.Fatalf("Unable to access given basePath '%s': %s\n", basePath, err)
	}
}

// getWorkingDir gets the current working dir or an empty string
// if it couldn't be retrieved.
func getWorkingDir() string {
	dir, err := os.Getwd()
	if err != nil {
		return ""
	}
	return dir
}

// checkBasePath checks whether the given base path can be used
// if not an error is returned.
func checkBasePath(path string) error {
	fi, err := os.Stat(path)
	if os.IsNotExist(err) {
		// Doesn't exist
		return err
	}
	if !fi.IsDir() {
		// Not a folder
		return fmt.Errorf("basepath '%s' isn't a folder", path)
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		// Error reading dir
		return err
	}
	if !containsRequiredFolders(files) {
		return requiredFolderError{
			files,
			fmt.Errorf("basepath doesn't contain required folders %s", neededFolders),
		}
	}
	return nil
}

type requiredFolderError struct {
	RequiredFolders []os.FileInfo
	error
}

// containsRequiredFolders checks whether the required folders are contained in
// given files
func containsRequiredFolders(files []os.FileInfo) bool {
	for _, name := range neededFolders {
		found := false
		for _, file := range files {
			if file.IsDir() && (file.Name() == name) {
				found = true
			}
		}
		if !found {
			return false
		}
	}
	return true
}
