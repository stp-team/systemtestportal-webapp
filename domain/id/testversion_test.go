/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

func TestNewTestEntityVersionID(t *testing.T) {
	owner := NewActorID("testUser")
	project := NewProjectID(owner, "Project-1")
	tc := NewTestID(project, "TestCase", true)
	versionNumbers := []int{
		1,
		0,
		-1,
		math.MaxInt64,
	}
	for _, versionNr := range versionNumbers {
		id := NewTestVersionID(tc, versionNr)
		if !reflect.DeepEqual(tc, id.TestID) {
			t.Errorf("Testcase wasn't saved correctly to the new TestVersionID. Expected %v but got %v.",
				tc, id.TestID)
		}
		if versionNr != id.TestVersion() {
			t.Errorf("VersionNr wasn't saved correctly to new TestVersionID. Expected %q but got %q.",
				versionNr, id.TestVersion())
		}
	}
}

type TestVersionExistenceCheckerStub struct {
	exists bool
	err    error
}

func (tvc TestVersionExistenceCheckerStub) Exists(TestVersionID) (bool, error) {
	return tvc.exists, tvc.err
}
func TestTestVersionID_Validate(t *testing.T) {
	test := NewTestID(NewProjectID(NewActorID("John"), "Ice-cream"), "test name", true)
	testData := []struct {
		versionNr     int
		returnExists  bool
		returnError   error
		errorExpected bool
	}{
		{-2, false, nil, true},
		{0, false, nil, true},
		{1, false, nil, false},
		{3, true, nil, true},
		{4, false, fmt.Errorf("i'm an error"), true},
	}
	for _, set := range testData {
		id := NewTestVersionID(test, set.versionNr)
		result := id.Validate(TestVersionExistenceCheckerStub{set.returnExists, set.returnError})
		if set.errorExpected && (result == nil) {
			t.Errorf("Validate returned no error, but expected one. \nID: %+v", id)
		}
		if !set.errorExpected && (result != nil) {
			t.Errorf("Validate returned an error, but expected none. \nID: %+v \nError: %v", id, result)
		}
	}
}

func TestTestVersionID_JSONUnMarshal(t *testing.T) {
	id := NewTestVersionID(NewTestID(NewProjectID(NewActorID("hello world!"), "It's me "), "Mario!", true), 4)
	json, err := id.MarshalJSON()
	if err != nil {
		t.Error(err)
	}
	newID := TestVersionID{}
	err = newID.UnmarshalJSON(json)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(id, newID) {
		t.Errorf("ID has changed while marshalling and unmarshalling. \nOriginal ID: %+v \nResult: %+v", id, newID)
	}
}
func TestTestVersionID_GobEnDecode(t *testing.T) {
	id := NewTestVersionID(NewTestID(NewProjectID(NewActorID("hello world!"), "It's me "), "Mario!", true), 4)
	data, err := id.GobEncode()
	if err != nil {
		t.Error(err)
	}
	newID := TestVersionID{}
	err = newID.GobDecode(data)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(id, newID) {
		t.Errorf("ID has changed while decoding and encoding. \nOriginal ID: %+v \nResult: %+v", id, newID)
	}
}
