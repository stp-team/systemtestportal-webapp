/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */



/**
 * Checks if the "Hide System Message" Cookie is present and removes the message, if so
 */
$(function () {

    const cookie = document.cookie;

    if (cookie.includes("hideSystemMessage")) {
        const regexp = /.*hideSystemMessage=(\d*);?.*/gmi;
        const cookieTimeStamp = regexp.exec(cookie)[1];

        if (cookieTimeStamp < messageTimeStamp) {
            showMessage();
        } else {
            hideMessage();
        }
    } else {
        showMessage();
    }
});


/**
 * Removes the message and sets a cookie to hide the message
 */
$("#remove-system-message").click(function() {
    hideMessage();
    document.cookie = "hideSystemMessage=" + Math.floor(Date.now() / 1000)+";path=/";
});

/**
 * Removes the message; Decreases the Padding
 */
function hideMessage() {
    $('#system-message').css("display", "none");
    $('body').css("marginTop", "0");
}

/**
 * Shows the message; Increases the Padding
 */
function showMessage() {
    if(!globalMessageExpires || globalMessageExpiration > Math.floor(Date.now() / 1000)) {
        $('#system-message').css("display", "flex");
        const messageHeight = toRem($('#system-message').css("height"));
        $('body').css("marginTop" , (messageHeight -3.5) + "rem");
    }
}