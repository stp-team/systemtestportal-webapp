{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<link rel="stylesheet" type="text/css" href="/static/css/project/reorder-utils.css" integrity="{{sha256 "/static/css/project/reorder-utils.css"}}">
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Create Test Case" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            {{T "You can fill in all the relevant data for a new test case in this form" .}}.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>{{T "Buttons" .}}</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Abort" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Aborts the creation process" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-success">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Save" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Confirms the form content and proceeds with creation" .}}.</td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>{{T "Form Fields" .}}</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Case Name" .}}</strong>
                        </td>
                        <td>{{T "Enter a short but descriptive name for the test case" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Case Description" .}}</strong>
                        </td>
                        <td>{{T "Describe the purpose of that test case in some sentences" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Case Preconditions" .}}</strong>
                        </td>
                        <td>{{T "State some preconditions that must be fulfilled prior to test case execution" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Steps" .}}</strong>
                        </td>
                        <td>{{T "Add some test steps here" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Versions & Variants" .}}</strong>
                        </td>
                        <td>{{T "Select some versions under which the test case can be executed for each relevant version" .}}.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Estimated Test Time" .}} (hh:mm)</strong>
                        </td>
                        <td>{{T "Enter an estimated amount of time that is needed to execute the whole test" .}}.</td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestCases">
    <form id="testCaseEdit">
        <script>var cases = {{.TestCases}};</script>
        <!-- Control buttons -->
        <nav class="navbar navbar-light action-bar p-3 d-print-none">
            <div class="input-group flex-nowrap">
                <button id="buttonAbort" class="btn btn-secondary" type="button">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> {{T "Abort" .}}</span>
                </button>

                <button id="buttonSaveTestCase" class="btn btn-success ml-2 mouse-hover-pointer" type="submit" disabled>
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> {{T "Save" .}}</span>
                </button>
            </div>
        </nav>
        <!-- Text inputs -->
        <div class="row tab-side-bar-row">
            <div class="col-md-9 p-3">
                <h4 class="mb-3">{{T "Create Test Case" .}}</h4>
                <div class="form-group">
                    <label for="inputTestCaseName"><strong>{{T "Test Case Name" .}}</strong></label>
                    <input id="inputTestCaseName" name="inputTestCaseName"
                           class="form-control" placeholder="{{T "Enter test case name" .}}"
>
                </div>
                <div class="form-group">
                    <label for="inputTestCaseDescription"><strong>{{T "Test Case Description" .}}</strong></label>
                    <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputTestCaseDescription" rows="4"
                                  placeholder="{{T "Describe the test case" .}}"></textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputTestCasePreconditions"><strong>{{T "Test Case Preconditions" .}}</strong></label>
                    <ul class="list-group" id="preconditionsList">
                     {{ if .Preconditions }} 
                        {{ range $index, $elem := .Preconditions }}
                        <li class="list-group-item preconditionItem" >
                            <span>{{ $elem.Content }}</span>
                            <button class="close ml-2 list-line-item btn-sm pull-right" type="button">
                                    <span class="d-none d-sm-inline">x</span>
                            </button>
                        </li>
                        {{ end }}
                    {{ end }} 
                    </ul>
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="preconditionAdder">
                            <i class="fa fa-plus"></i>
                        </button>
                        </span>
                        <input class="form-control width100" id="preconditionInput" placeholder="{{T "New Precondition" .}}">
                    </div>
                </div>

                <label class="mt-2 mb-0" for="inputTestSteps"><strong>{{T "Test Steps" .}}</strong></label>
                <button type="button" class="btn btn-sm btn-primary float-right" id="buttonAddTestStep"
                        data-toggle="modal" data-target="#modal-teststep-edit">
                    {{T "Add Test Step" .}}
                </button>

                <div id="test-steps-container" class="form-group">
                    <ul class="list-group sortable pb-2 pt-2" id="testStepsAccordion" data-children=".li"></ul>
                    {{template "modal-teststep-edit" .}}
                </div>
            </div>

            <!-- Right side inputs -->
            <div class="col-md-3 p-3 tab-side-bar">
                <div class="form-group">
                    <label>
                        <strong>{{T "Applicability" .}}</strong>
                        <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-versions">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                        </button>
                    </label>

                    {{ if .Project.Versions }}
                    <div id="someversions" class="form-group">
                    {{ else }}
                    <div id="someversions" class="form-group d-none">
                    {{ end }}
                        <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                            <label for="inputTestCaseSUTVersions">
                                <strong>{{T "Versions" .}}</strong>
                            </label>
                        </div>

                        <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                            <select class="custom-select mb-2" id="inputTestCaseSUTVersions"></select>
                        </div>

                        <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                            <label for="inputTestCaseSUTVariants">
                                <strong>{{T "Variants" .}}</strong>
                            </label>
                        </div>

                        <div class="form-group col-10 col-sm-11 col-md-12 col-lg-12">
                            <select multiple class="form-control" id="inputTestCaseSUTVariants"></select>
                        </div>
                    </div>

                    {{ if .Project.Versions }}
                    <div id="noversion" class="form-group d-none">
                    {{ else }}
                    <div id="noversion" class="form-group">
                    {{ end }}
                        <p class="text-muted">{{T "The project does not have any versions yet. Click on the icon above to manage the versions" .}}.</p>
                    </div>
                </div>

                <div class="form-group mt-4">
                    <label for="inputHours"><strong>{{T "Estimated Test Time" .}} (hh:mm)</strong></label>
                    <div class="row m-0">
                        <div class="col-lg-6 col-md-12 col-sm-3 col-6 p-0 pb-md-2">
                            <div class="input-group mb-2 mb-sm-0 ">
                                <input type="number" class="form-control pt-0 pb-0" id="inputHours" placeholder="h"
                                       min="0" oninput="checkHours(this)">
                                <div class="btn-group-vertical">
                                    <button type="button" id="hour-plus" class="input-group-addon btn btn-primary">
                                        <span>&#x2b;</span>
                                    </button>
                                    <button type="button" id="hour-minus" class="input-group-addon btn btn-primary">
                                        <span>&#x2212;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-3 col-6 p-0 pl-2 pl-md-0 pl-lg-2">
                            <div class="input-group mb-2 mb-sm-0 ">
                                <input type="number" class="form-control pt-0 pb-0" id="inputMinutes"
                                       placeholder="min"
                                       min="0" max="59" oninput="checkMins(this)">
                                <div class="btn-group-vertical">
                                    <button type="button" id="minute-plus"
                                            class="input-group-addon btn btn-primary">
                                        <span>&#x2b;</span>
                                    </button>
                                    <button type="button" id="minute-minus"
                                            class="input-group-addon btn btn-primary">
                                        <span>&#x2212;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label><strong>{{T "Labels" .}}</strong></label>
                    <span class="editLabels">
                    {{if .User}}
                        <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-labels" >
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                        </button>
                        <div class="form-group">
                            <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                                <strong>{{T "Assign" .}}</strong>
                            </div>

                            <!-- dirty hack -->
                            <form class="form-inline"></form>

                            <div class="col-10 col-sm-11 col-md-12 col-lg-12" style="margin-top:0.5rem">
                                <div class="" style="overflow-y: scroll; max-height: 212px;border: 1px solid #ced4da;border-radius: .25rem;">
                                    <div id="assignLabelContainer" class="col-12">
                                    {{range .Project.Labels}}
                                        <div id="assign-container-item-{{ .Id }}"
                                             class="input-group mb-3"
                                             onclick="onShowLabelClick({{ .Id }})"
                                             style="margin-top: 1rem;margin-bottom: 0rem;cursor: pointer;">
                                            <form class="form-inline">
                                                <div class="form-group">
                                                    <div class="input-group-prepend">
                                                        <i id="assign-container-icon-{{ .Id }}"
                                                           class="fa input-group-text"
                                                           data-toggle="tooltip"
                                                           data-original-title="Click to assign/unassign"
                                                           style="padding: 0.9rem"></i>
                                                    </div>
                                                    <span id="assign-container-label-{{ .Id }}"
                                                          class="badge badge-primary clickIcon"
                                                          data-toggle="tooltip"
                                                          data-original-title="{{ .Description }}"
                                                          style="background-color: {{ .Color }};
                                                                  color: {{.TextColor}};
                                                                  line-height: 2">{{ .Name }}</span>
                                                </div>
                                            </form>
                                        </div>
                                    {{end}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{end}}
                    </span>

                    <p id="showLabelContainer">
                    {{ range .Project.Labels }}
                        <span   id="show-container-label-{{ .Id }}"
                                class="badge badge-primary clickIcon"
                                data-toggle="tooltip"
                                data-original-title="{{ .Description }}"
                                style="background-color: {{ .Color }};
                                        color: {{.TextColor}};
                                        display: none">{{ .Name }}</span>
                    {{ end }}
                        <span id="showContainerText" class="text-muted">{{T "No Labels" .}}</span>
                    </p>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" id="modal-confirm-case-save-without-steps" tabindex="-1" role="dialog"
     aria-labelledby="modal-confirm-case-save-without-steps-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-confirm-case-save-without-steps-label">{{T "Save Test Case" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p>{{T "Do you really want to save the test case without any steps?" .}}</p>
                </div>
            </div>
            <div class="modal-footer">
                <button id="buttonConfirmSaveWithoutSteps" type="button" class="btn btn-primary">{{T "Save" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>

{{template "modal-manage-versions" . }}
{{template "modal-manage-labels" . }}


<!-- Import Scripts here -->
<script src="/static/js/project/testcases.js" integrity="{{sha256 "/static/js/project/testcases.js"}}"></script>
<script src="/static/js/project/sut-versions.js" integrity="{{sha256 "/static/js/project/sut-versions.js"}}"></script>
<script src="/static/js/project/tests.js" integrity="{{sha256 "/static/js/project/tests.js"}}"></script>
<script src="/static/assets/js/vendor/jquery.validate.min.js" integrity="{{sha256 "/static/assets/js/vendor/jquery.validate.min.js"}}"></script>

<!-- Scripts needed for labels -->
<script>
    // Modal mode controls how the manage label modal interacts with its parent. 1 = create + show view
    modalMode = 1;
    isInCreateMode = true;

    testAssignedLabelIds = [ ];

    setShowLabelText();
    setShowLabelTextForAssignments();
</script>


<script>
    assignEventsToTextFields();
    assignButtonsTestCase();
    assignPreconditionInputListener();
</script>

<script type="text/javascript">
    //save labels of project
    var projectLabels = {{ .Project.Labels }}
            // Initialize bootstrap multiselect
            $(document).ready(function () {
                $('#labels').multiselect();
            });

    reloadDragAndDropFeature()
</script>
{{end}}


