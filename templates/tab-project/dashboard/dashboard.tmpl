{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "tab-content"}}
{{template "modal-generic-error" .}}

<div class="tab-card card" id="tabTestDashboard">

    <!-- Nav Bar -->
    <nav class="navbar navbar-light action-bar p-3 d-print-none">

        <div class="input-group" style="margin-top: -10px">

            {{/*Search*/}}
            <div class="mr-auto" style="margin-top: 10px">
                <input id="dashboard-search" style="width: 100%;" type="search" name="s" class="search-field form-control"
                       placeholder="{{T "Search..." .}}">
            </div>

            <div class="d-inline-flex align-items-center mr-3" style="margin-top: 10px">

                {{/*Variant selection*/}}
                <div class="control-label pr-2">
                    <strong>{{T "Variant" .}}</strong>
                </div>

                <select class="selectpicker custom-select rounded" data-style="btn-primary" id="versions-dropdown-menu" name="versions-dropdown-menu" title="">
                    {{/*get filled by fillDropdown*/}}
                </select>

            </div>

            {{/*Case/Sequence toggle*/}}
            <div style="margin-top: 10px">
                <input id="dashboardToggle" checked data-toggle="toggle" data-on="Test Cases" data-off="Test Sequences" data-offstyle="primary" type="checkbox" title="">
            </div>

        </div>

    </nav>

    <!-- Content -->
    <div class="row tab-side-bar-row">
        <div class="col-md-12 p-3">
            <h4 class="mb-3">
                {{T "Dashboard" .}}
            </h4>
            <div class="panel panel-default table-responsive">
                <table class="table table-bordered" id="dashboardTable">
                    <thead>
                        <tr id="variantNames">
                            <th>
                                {{T "Test Cases" .}}
                            </th>
                        </tr>
                    </thead>
                    <tbody id="dashboard-content">
                        {{/*get filled by onChangeDropdown and newSiteDashboard*/}}
                        <td colspan="5" class="text-center">
                            ... {{T "Loading" .}} ...
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/dashboard.js" integrity="{{sha256 "/static/js/project/dashboard.js"}}"></script>

<script>

    //Define strings for JavaScript here
    var stringTestCases = "" + {{T "Test Cases" .}};
    var stringTestSequences = "" + {{T "Test Sequences" .}};
    var stringTestSequenceNotYetExecuted = "" + {{T "Test sequence not yet executed" .}};
    var stringNotAssessed = "" + {{T "Not assessed"}};
    var stringPassed = "" + {{T "Passed" .}};
    var stringPartiallySuccessful = "" + {{T "Partially Successful" .}};
    var stringFailed = "" + {{T "Failed" .}};
    var stringTestSequenceIsNotApplicable = "" + {{T "Test sequence is not applicable for this version" .}};
    var stringTestCaseNotYetExecuted = "" + {{T "Test case not yet executed" .}};
    var stringTestCaseIsNotApplicable = "" + {{T "Test case is not applicable for this version" .}};
    var stringResult = "" + {{T "Result" .}};
    var stringTester = "" + {{T "Tester" .}};
    var stringComment = "" + {{T "Comment" .}};
    var stringDate = "" + {{T "Date" .}};
    var stringNoComment = "" + {{T "No comment" .}};

    $("#printerIcon").addClass("d-none");
    $("#helpIcon").addClass("d-none");
    var casesDashboardElements = {{.Dashboard.DashboardElements}};
    var sequencesDashboardElements = {{.DashboardSequences.DashboardElements}};
    var versions = {{.Dashboard.Versions}};
    var variants = {{.Dashboard.Variants}};

    fillDropdown(variants);

    if ($(dashboardToggle).prop('checked')) {
        newSiteDashboard(casesDashboardElements, versions, variants, true);
    } else {
        newSiteDashboard(sequencesDashboardElements, versions, variants, false);
    }

    // On document ready
    $(() => {
        $(dashboardToggle).bootstrapToggle({
            on: stringTestCases,
            off: stringTestSequences,
            offstyle: "primary"
        });

        initDataTable();
    });

</script>
{{end}}