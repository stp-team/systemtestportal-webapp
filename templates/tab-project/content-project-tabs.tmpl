{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "content"}}
        {{$Option := 1}}
        {{if .User}}
            {{if eq .User.ProjectHeaderMode 1}}
            <div class="row d-sm-flex d-none mt-2 mb-4">
                <div class="col-sm-2 d-none d-sm-block d-print-none">
                    <div class="d-flex project-small-thumbnail mouse-hover-pointer" id="projectImage">
                        <img src="{{getImagePath .Project.Image "project"}}" class="rounded" alt="project logo">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10">
                    <div class="card-block">
                        <div class="d-none d-sm-block float-right badge badge-secondary d-print-none"><span>{{T "created" .}} </span><time class="timeago" datetime="{{ provideTimeago .Project.CreationDate }}"></time></div>
                        <h4 class="card-title">{{ .Project.Owner }}/{{ .Project.Name }}</h4>
                        <p class="card-text">{{printMarkdown .Project.Description }}</p>
                    </div>
                </div>
            </div>
            {{else if eq .User.ProjectHeaderMode 2}}
            <div class="row d-sm-flex d-none mt-1 mb-2">
                <div class="col-sm-8 font-weight-bold w-100">
                    <img src="{{getImagePath .Project.Image "project"}}" class="rounded" alt="project logo" style="width: 50px; height: 50px;">
                    <i class="fa fa-angle-right mr-1 ml-1"></i><a href="/users/{{ .Project.Owner }}">{{ .Project.Owner }}</a><i class="fa fa-angle-right mr-1 ml-1"></i><a href="/{{ .Project.Owner }}/{{ .Project.Name }}">{{ .Project.Name }}</a>
                </div>
                <div class="col-sm-4 align-baseline">
                    <div class="d-none d-sm-block float-right badge badge-secondary d-print-none" style="max-height: 1.125rem;"><span>{{T "created" .}} </span><time class="timeago" datetime="{{ provideTimeago .Project.CreationDate }}"></time></div>
                </div>
            </div>
            {{else}}
            <div class="row d-sm-flex d-none mt-2 mb-4">

            </div>
            {{end}}
        {{else}}
            <div class="row d-sm-flex d-none mt-2 mb-4">
                <div class="col-sm-2 d-none d-sm-block d-print-none">
                    <div class="d-flex project-small-thumbnail mouse-hover-pointer" id="projectImage">
                        <img src="{{getImagePath .Project.Image "project"}}" class="rounded" alt="project logo">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10">
                    <div class="card-block">
                        <div class="d-none d-sm-block float-right badge badge-secondary d-print-none"><span>{{T "created" .}} </span><time class="timeago" datetime="{{ provideTimeago .Project.CreationDate }}"></time></div>
                        <h4 class="card-title">{{ .Project.Owner }}/{{ .Project.Name }}</h4>
                        <p class="card-text">{{printMarkdown .Project.Description }}</p>
                    </div>
                </div>
            </div>
        {{end}}

        <!-- Nav tabs -->
        <nav class="mobile-full-width d-print-none" role="navigation">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="tabButtonDashboard" data-toggle="tab" href="" role="tab">{{T "Dashboard" .}}</a>
                </li>
                <li class="nav-item hide-5">
                    <a class="nav-link" id="tabButtonActivity" data-toggle="tab" href="" role="tab">{{T "Activity" .}}</a>
                </li>
                <li class="nav-item hide-4">
                    <a class="nav-link" id="tabButtonTestCases" data-toggle="tab" href="" role="tab">{{T "Test Cases" .}}</a>
                </li>
                <li class="nav-item hide-3">
                    <a class="nav-link" id="tabButtonTestSequences" data-toggle="tab" href="" role="tab">{{T "Test Sequences" .}}</a>
                </li>
                <li class="nav-item hide-2">
                    <a class="nav-link" id="tabButtonProtocols" data-toggle="tab" href="" role="tab">{{T "Protocols" .}}</a>
                </li>
                <li class="nav-item hide-1">
                    <a class="nav-link" id="tabButtonMembers" data-toggle="tab" href="" role="tab">{{T "Members" .}}</a>
                </li>
            {{ if or .ProjectPermissions.EditProject .Admin }}
                <li class="nav-item hide-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                    <a class="nav-link" id="tabButtonSettings" data-toggle="tab" href="" role="tab">{{T "Settings" .}}</a>
                </li>
            {{else}}
                <li class="nav-item" data-toggle="tooltip" data-placement="bottom" title="" data-original-title={{T "You do not have the rights to access the settings" .}}>
                    <a class="nav-link disabled" href="#" id="tabButtonSettings">{{T "Settings" .}}</a>
                </li>
            {{ end }}
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="more" href="" role="button">{{T "More" .}}</a>
                    <div class="dropdown-menu tab-collapse-menu">
                        <a class="dropdown-item show-6" data-toggle="tab" id="menuButtonDashboard">{{T "Dashboard" .}}</a>
                        <a class="dropdown-item show-5" data-toggle="tab" id="menuButtonActivity"> Activity </a>*
                        <a class="dropdown-item show-4" data-toggle="tab" id="menuButtonTestCases">{{T "Test Cases" .}}</a>
                        <a class="dropdown-item show-3" data-toggle="tab" id="menuButtonTestSequences">{{T "Test Sequences" .}}</a>
                        <a class="dropdown-item show-2" data-toggle="tab" id="menuButtonProtocols">{{T "Protocols" .}}</a>
                        <a class="dropdown-item show-1 " data-toggle="tab" id="menuButtonMembers">{{T "Members" .}}</a>
                        {{ if or .Member .Admin }}
                            <a class="dropdown-item show-1" data-toggle="tab" id="menuButtonSettings">{{T "Settings" .}}</a>
                        {{ end }}
                    </div>
                </li>
                <li id="helpIcon" class="nav-item ml-auto">
                    <a class="nav-link disabled No-Warning-On-Current-Action-Abort" id="tabButtonHelp" data-toggle="modal"
                       data-target="#helpModal" href="" role="tab">
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle = "tooltip" data-placement = "bottom" data-original-title="Help"></i>
                    </a>

                </li>
                <li id="printerIcon" class="nav-item ml-0 d-none">
                    <a class="nav-link disabled No-Warning-On-Current-Action-Abort" id="tabButtonPrint" onClick="printer();" role="tab" style="cursor: pointer;">
                        <i class="fa fa-print" aria-hidden="true" data-toggle = "tooltip" data-placement = "bottom" data-original-title="Print"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- Tab panes -->
        <div class="tab-content mobile-full-width">
            <div class="tab-pane active" id="tabarea" role="tabpanel">
                {{template "tab-content" . }}
            </div>
        </div>

        <!-- Import Scripts here -->
        <script src="/static/js/project/project-tabs.js" integrity="{{sha256 "/static/js/project/project-tabs.js"}}"></script>

        <script>
            initializeTabClickListener();
        </script>
{{end}}


