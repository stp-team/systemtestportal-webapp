/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package task

import (
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestListGet(t *testing.T) {
	store.InitializeTestDatabase()
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: nil,
		},
	)

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{}
				return GetTask(taskListGetterMock, false), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Getter returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{
					Err:      errors.New("error getting task-list"),
					TaskList: &task.List{},
				}
				return GetTask(taskListGetterMock, true), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Valid request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{
					TaskList: &task.List{},
				}
				return GetTask(taskListGetterMock, true), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestItemPut(t *testing.T) {
	store.InitializeTestDatabase()
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: nil,
		},
	)

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	invalidBody := "INVALIDNR"

	body := "1"

	handler.Suite(t,
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{}
				taskListAdderMock := handler.TaskListAdderMock{}
				return ItemPut(taskListGetterMock, taskListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodPut, handler.NoParams),
		),
		handler.CreateTest("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{}
				taskListAdderMock := handler.TaskListAdderMock{}
				return ItemPut(taskListGetterMock, taskListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, invalidBody),
		),
		handler.CreateTest("Getter returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{
					Err: errors.New("error getting task list for user"),
				}
				taskListAdderMock := handler.TaskListAdderMock{}
				return ItemPut(taskListGetterMock, taskListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{
					TaskList: &task.List{
						Username: "",
						Tasks: []task.Item{
							{
								Index: 1,
							},
						},
					},
				}
				taskListAdderMock := handler.TaskListAdderMock{
					Err: errors.New("error adding task-list to store"),
				}
				return ItemPut(taskListGetterMock, taskListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Valid request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				taskListGetterMock := handler.TaskListGetterMock{
					TaskList: &task.List{
						Username: "",
						Tasks: []task.Item{
							{
								Index: 1,
							},
						},
					},
				}
				taskListAdderMock := handler.TaskListAdderMock{}
				return ItemPut(taskListGetterMock, taskListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
	)
}
