/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"encoding/json"
	"net/http"
	"strconv"

	"regexp"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// TestCaseInput contains everything needed to create a testcase.
type TestCaseInput struct {
	InputTestCaseName          string
	InputTestCaseDescription   string
	InputTestCasePreconditions []string
	InputTestCaseSUTVersions   map[string]*project.Version
	InputHours                 int
	InputMinutes               int
	InputSteps                 []InputStep
	InputLabels                []string
}

// InputStep represents an step of the input data
type InputStep struct {
	ID               int
	Actual, Expected string
}

// CasePost returns a function that handles the creation of a case
func CasePost(ta handler.TestCaseAdder, caseChecker id.TestExistenceChecker, labelStore handler.Labels, activityStore handler.Activities) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).CreateCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		input, err := getCaseInput(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tc, err := createCase(c.Project.ID(), *input)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err := addCase(ta, caseChecker, tc); err != nil {
			errors.Handle(err, w, r)
			return
		}

		// Because we only use context/activity entities in activity-items, we need to set the new tc into the context
		c.Case = tc
		activityStore.LogActivity(activity.CREATE_CASE, c.GetActivityEntities(), r)

		// ADD THE NEW ASSIGNED LABELS
		for _, labelIdString := range input.InputLabels {
			labelId, err := strconv.ParseInt(labelIdString, 10, 64)
			if err != nil {
				errors.Handle(err, w, r)
			}

			labelStore.InsertTestLabel(labelId, tc.Id, c.Project.Id, true)
		}

		httputil.SetHeaderValue(w, httputil.NewName, tc.ItemName())
		http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
	}
}

// getCaseInput gets the test case input from the request.
// If an error occurs the returned input will be nil.
func getCaseInput(r *http.Request) (*TestCaseInput, error) {
	input := TestCaseInput{}
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, err
	}
	return &input, nil
}

// createCase creates a new case from user input.
// If the creation fails an error is returned and the case will be nil.
func createCase(pID id.ProjectID, input TestCaseInput) (*test.Case, error) {
	dur, err := duration.GetDuration(input.InputHours, input.InputMinutes)
	if err != nil {
		return nil, err
	}

	st := ConstructSteps(input.InputSteps)

	//Trim all leading and trailing whitespaces and replace multiple whitespaces with a single space character
	input.InputTestCaseName = strings.TrimSpace(input.InputTestCaseName)
	re := regexp.MustCompile(`\s\s+`)
	input.InputTestCaseName = re.ReplaceAllString(input.InputTestCaseName, " ")

	preconditions := handlePreconditions(input.InputTestCasePreconditions)
	tc := test.NewTestCase(
		input.InputTestCaseName,
		input.InputTestCaseDescription,
		preconditions,
		input.InputTestCaseSUTVersions,
		dur,
		pID,
	)
	tc.TestCaseVersions[0].Steps = st

	return &tc, nil
}

// addCase adds a new case to the storage.
func addCase(ta handler.TestCaseAdder, caseChecker id.TestExistenceChecker, tc *test.Case) error {
	if vErr := tc.ID().Validate(caseChecker); vErr != nil {
		return vErr
	}

	return ta.Add(tc)
}

// ConstructSteps creates steps from user input.
// Returns an empty array in no test steps were given.
func ConstructSteps(rawSteps []InputStep) []test.Step {
	if rawSteps == nil {
		return nil
	}
	var resSteps []test.Step
	for i, s := range rawSteps {
		resSteps = append(resSteps, test.NewTestStep(s.Actual, s.Expected))
		resSteps[i].Index = s.ID
	}
	return resSteps
}

func handlePreconditions(input []string) []test.Precondition {
	preconditions := make([]test.Precondition, 0)

	for _, prec := range input {
		preconditions = append(preconditions, test.Precondition{
			Content: prec,
		})
	}
	return preconditions
}
