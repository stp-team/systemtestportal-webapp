/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// GroupsGet serves the page that is used to explore groups.
func GroupsGet(g handler.GroupLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		list, err := g.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getExploreGroupsTree(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Groups, list), tmpl, w, r)
	}
}

// getExploreGroupsTree returns the list groups template with all parent templates
func getExploreGroupsTree(r *http.Request) *template.Template {
	return handler.GetSideBarTree(r).
		// Explore projects tree
		Append(templates.ExploreGroups).
		Get().Lookup(templates.HeaderDef)
}
