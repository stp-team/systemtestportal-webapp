/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"github.com/dimfeld/httptreemux"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/usersession"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

func registerAccountHandler(r *httptreemux.ContextMux) {
	userStore := store.GetUserStore()
	sessionStore := sessions.GetSessionStore()

	r.GET(Register, display.RegisterUserGet)

	r.Handler(http.MethodPost, SignIn, usersession.NewSigninHandler(sessionStore, userStore))
	r.Handler(http.MethodPost, SignOut, usersession.NewSignoutHandler(sessionStore))
	r.Handler(http.MethodPost, Register, creation.NewRegisterHandler(userStore))
}
